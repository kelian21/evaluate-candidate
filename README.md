# Steps for the candidate
1. Create an account with https://bitbucket.org.
2. Please provide us with the email you used when you signed up for the BitBucket account. We need it to add you to the team.
3. Open the file named after you; translate the sentence in the file to a language that you are going to work with.
4. Commit the file.

That's it!
